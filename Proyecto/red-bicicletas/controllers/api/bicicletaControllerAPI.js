var Bicicleta = require('../../models/bicicleta');

exports.bicicleta_list = function(req, res){
    Bicicleta.find({}, function(err, bicis){
        res.status(200).json({
            bicicletas:bicis
        });
    });
};

exports.bicicleta_create = function(req, res){
    var bici = Bicicleta.createInstance(req.body.code, req.body.color, req.body.modelo);
    bici.ubicacion = [req.body.lat, req.body.long];
    Bicicleta.add(bici);

    res.status(200).json({
        bicicleta: bici
    });
};

exports.bicicleta_delete = function(req, res){
    Bicicleta.RemovebyId(req.body.code);
    res.status(204).send();
};