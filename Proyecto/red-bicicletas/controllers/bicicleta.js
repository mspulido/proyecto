var Bicicleta = require('../models/bicicleta');

exports.bicicleta_list = function(req, res){
    Bicicleta.find({}, function(err, bicis){
        res.render('bicicletas/index', {
            bicis:bicis
        });
    });
};

exports.bicicleta_create_get = function(req, res){
    res.render('bicicletas/create');
};

exports.bicicleta_create_post = function(req, res){
    var bici = Bicicleta.createInstance(req.body.code, req.body.color, req.body.modelo, [req.body.lat, req.body.long]);
    Bicicleta.add(bici);
    res.redirect('/bicicletas');
};

exports.bicicleta_delete_post = function(req, res){
    Bicicleta.removeByCode(req.params._id, function(err){
        if (err) return console.log(err)
        res.redirect('/bicicletas');
    });
};

exports.bicicleta_update_get = function(req, res){
    Bicicleta.findByCode(req.params._id, function(err, bici){
        res.render('bicicletas/update', {bici});
    });
};

exports.bicicleta_update_post = function(req, res){
    var update_values = {code: req.body.code, color: req.body.color, modelo: req.body.modelo, ubicacion: [req.body.lat, req.body.long] };
    Bicicleta.findByIdAndUpdate(req.params._id, update_values, function(err, bici){
        if(err){
            console.log(err);
            res.render('bicicletas/update', {errors: err.errors, bicicleta: new Bicicleta({code: req.body.code, color: req.body.color, modelo: req.body.modelo, ubicacion: [req.body.lat, req.body.long]})});
        }else{
            res.redirect('/bicicletas');
            return;
        }
    })    
    ;
    
};