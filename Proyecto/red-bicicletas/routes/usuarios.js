var express = require('express');
var router = express.Router();
var UsuariosController = require('../controllers/usuarios');

router.get('/', UsuariosController.list);
router.get('/create', UsuariosController.create_get);
router.post('/create', UsuariosController.create);
router.post('/:id/delete', UsuariosController.delete);
router.get('/:id/update', UsuariosController.update_get);
router.post('/:id/update', UsuariosController.update);

module.exports = router;