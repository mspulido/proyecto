var server = require('../../bin/www');
var Bicicleta = require('../../models/bicicleta');
var request = require('request');
var mongoose = require('mongoose');

var base_url = "http://localhost:3000/api/bicicletas";

describe('Bicicleta Api', () => {
    beforeEach(function(done){
        var mongo = process.env.MONGO_URI;
        mongoose.createConnection(mongo, { useNewUrlParser: true });
        mongoose.Promise = global.Promise;
        const db = mongoose.connection;
        db.on('error', console.error.bind(console, 'connection error'));
        db.once('open', function(){
            console.log('we are connected to test data base');
        });
        Bicicleta.deleteMany({}, function(err, success){
            if (err) console.log(err);
            done();
        });
    });

    describe('POST Bicicletas /create', () => {
        it('Status 200', (done) => {

            var headers = {'content-type' : 'application/json'};
            var abicis = '{ "code": 10, "color": "rojo", "modelo": "montaña", "lat": -14, "long": -34 }';
            request.post({
                headers: headers,
                url: base_url + '/create',
                body: abicis
                }, function(error, response, body){
                 expect(response.statusCode).toBe(200);
                 var bici = JSON.parse(body).bicicleta;
                 expect(bici.color).toBe('rojo');
                 expect(bici.ubicacion[0]).toBe(-14);
                 expect(bici.ubicacion[1]).toBe(-34);

                 done();
            });
        });
    });

    describe('GET Bicicletas /', () => {
        it('Status 200', (done) => {
            request.get(base_url, function(error, response, body){
                var bici = JSON.parse(body);
                console.log(bici);
                expect(response.statusCode).toBe(200);
                expect(bici.bicicletas.length).toBe(0);
                done();
            });
        });
    });


});