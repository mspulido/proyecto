var mongoose = require('mongoose');

var Bicicleta = require('../../models/bicicleta');



describe('Testing Bicicletas', () => {
    beforeEach(function(done){
        var mongo = process.env.MONGO_URI;
        mongoose.connect(mongo, { useNewUrlParser: true });
        mongoose.Promise = global.Promise;
        const db = mongoose.connection;
        db.on('error', console.error.bind(console, 'connection error'));
        db.once('open', function(){
            console.log('we are connected to test data base');
        });
        Bicicleta.deleteMany({}, function(err, success){
            if (err) console.log(err);
            done();
        });
    });

    describe('Bicicleta.allBicis', () => {
        it('Comienza vacia', (done) => {
            Bicicleta.allBicis(function(err, bicis){
                expect(bicis.length).toBe(0);
                done();
            });
        });
    });

    describe('Bicicleta.createInstance', () => {
        it('crea una instancia de Bicicleta', (done) => {
            var bici = Bicicleta.createInstance(1, "verde", "urbana", [-34.5, -45.3]);

            expect(bici.code).toBe(1);
            expect(bici.color).toBe('verde');
            expect(bici.modelo).toBe('urbana');
            expect(bici.ubicacion[0]).toBe(-34.5);
            expect(bici.ubicacion[1]).toBe(-45.3); 

            done();
        });
    });

    describe('Bicicleta.Add', () => {
        it('Agrega una bicicleta Bicicleta', (done) => {
            var bici = new Bicicleta({code:1, color:"verde", modelo:"urbana"});
            Bicicleta.add(bici, function(err, newBici){
                if (err) console.log(err);
                Bicicleta.allBicis(function(err, bicis){
                    expect(bicis.length).toEqual(1);
                    expect(bicis[0].code).toEqual(bici.code);

                    done();
                });
            });
        });
    });

    describe('Bicicleta.findByCode', () => {
        it('encuentra una bicicleta Bicicleta con codigo 1', (done) => {
            Bicicleta.allBicis(function(err, bicis){
                expect(bicis.length).toBe(0);

                var aBici = new Bicicleta({code:1, color:"verde", modelo:"urbana"});
                Bicicleta.add(aBici, function(err, newBici){
                    if (err) console.log(err);

                    var aBici2 = new Bicicleta({code:2, color:"rojo", modelo:"montaña"});
                    Bicicleta.add(aBici2, function(err, newBici){
                        if (err) console.log(err);

                        Bicicleta.findByCode(1, function(err, targetBici){
                            expect(targetBici.code).toBe(aBici.code);
                            expect(targetBici.color).toBe(aBici.color);
                            expect(targetBici.modelo).toBe(aBici.modelo);

                            done();
                        });
                    });
                });
            });
        });
    });

    describe('Bicicleta.removeByCode', () => {
        it('borra una bicicleta Bicicleta con codigo 1', (done) => {
            Bicicleta.allBicis(function(err, bicis){
                expect(bicis.length).toBe(0);

                var aBici = new Bicicleta({code:1, color:"verde", modelo:"urbana"});
                Bicicleta.add(aBici, function(err, newBici){
                    if (err) console.log(err);

                    var aBici2 = new Bicicleta({code:2, color:"rojo", modelo:"montaña"});
                    Bicicleta.add(aBici2, function(err, newBici){
                        if (err) console.log(err);

                        Bicicleta.allBicis(function(err, abicis){
                            expect(abicis.length).toBe(2);

                            Bicicleta.removeByCode(1, function(err, targetBici){
                                Bicicleta.allBicis(function(err, bbicis){
                                    if (err) console.log(err);
                                    expect(bbicis.length).toBe(1);
                                    expect(bbicis[0].code).toEqual(aBici2.code);
                                    expect(bbicis[0].color).toEqual(aBici2.color);
                                    expect(bbicis[0].modelo).toEqual(aBici2.modelo);

                                    done();
                                });
                            });
                        });
                    });
                });
            });
        });
    });

});






/*
beforeEach(() => {Bicicleta.allBicis = []});

describe('Bicicleta.allBicis', () => {
    it('Comienza vacia', () => {
        expect(Bicicleta.allBicis.length).toBe(0);
    });
});

describe('Bicicleta.add', () => {
    it('Agrega una bici', () => {
        expect(Bicicleta.allBicis.length).toBe(0);
        var a = new Bicicleta(2, 'blanco', 'urbana', [-34.909542,-57.950011]);
        Bicicleta.add(a);
        expect(Bicicleta.allBicis.length).toBe(1);
        expect(Bicicleta.allBicis[0]).toBe(a);
    });
});

describe('Bicicleta.FindbyId', () => {
    it('debe devolver la bici con id 1', () => {
        expect(Bicicleta.allBicis.length).toBe(0);
        var a = new Bicicleta(1, 'blanco', 'urbana', [-34.909542,-57.950011]);
        var b = new Bicicleta(2, 'rojo', 'montaña', [-34.909542,-57.950011]);
        Bicicleta.add(a);
        Bicicleta.add(b);
        var targetBici = Bicicleta.FindbyId(1);
        expect(targetBici.id).toBe(1);
        expect(targetBici.color).toBe(a.color);
        expect(targetBici.modelo).toBe(a.modelo);
    });
});

describe('Bicicleta.RemovebyId', () => {
    it('se remueve la bici con id 1', () => {
        expect(Bicicleta.allBicis.length).toBe(0);
        var a = new Bicicleta(1, 'blanco', 'urbana', [-34.909542,-57.950011]);
        Bicicleta.add(a);
        expect(Bicicleta.allBicis.length).toBe(1);
        Bicicleta.RemovebyId(1);
        expect(Bicicleta.allBicis.length).toBe(0);
    });
});
*/